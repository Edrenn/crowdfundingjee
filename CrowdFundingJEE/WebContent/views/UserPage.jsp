<%@include file="../common/header.jsp" %>

<h1>Profil de ${user.username}</h1>
<div class="col-md-6">
<form method="post">

	<div><label for="password">Mot de passe</label>
	<input type="password" name="password" /></div>
	
	<div><label for="password">V�rification du mot de passe</label>
	<input type="password" name="verifPassword" /></div>
	<c:if test="${MDPFailed == true}">
    <h5>Les mots de passe doivent correspondre.</h5>
    </c:if>
	
	<div><label for="email">Email</label>
	<input type="email" name="email" value="${user.email}" /></div>
	<c:if test="${MailAlreadyExists == true}">
    <h5>Cet email est d�j� utilis�.</h5>
    </c:if>
	<div><label for="firstname">Pr�nom</label>
	<input type="text" name="firstname" value="${user.firstname}"/></div>
	
	<div><label for="lastname">Nom de famille</label>
	<input type="text" name="lastname" value="${user.lastname}"/></div>
	
	<div><label for=imageUrl>Adresse de l'image :</label>
	<input type="text" name="imageUrl" value="${user.imageUrl}"/></div>
	
	<input type="submit" value="UserUpdate" />
</form>
</div>

<div class="col-md-6" >
<div><label for="imageUrl">Image de profil (URL)</label></div>
<div  class="showUser__image"><img src="${user.imageUrl}"></div>
</div>
<%@include file="../common/footer.jsp" %>
