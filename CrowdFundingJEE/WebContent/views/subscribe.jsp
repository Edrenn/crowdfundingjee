<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<jsp:include page="../common/header.jsp"></jsp:include>

<form action="subscribe" method="post">

	<label for="login">Login :</label>
	<input type="text" name="login" placeholder="Votre login" />
	<label for="password">Mot de passe</label>
	<input type="password" name="password" />
	<label for="email">Email</label>
	<input type="email" name="email" placeholder="Votre email" />
	<label for="firstname">Prénom</label>
	<input type="text" name="firstname" placeholder="Votre prénom"/>
	<label for="lastname">Nom de famille</label>
	<input type="text" name="lastname" placeholder="Votre nom de famille"/>
	<label for="imageUrl">Image de profil (URL)</label>
	<input type="text" name="imageUrl" placeholder="L'url de votre image"/>
	<input type="submit" value="Inscription" />
</form>
	<c:if test="${LoginAlreadyExists == true}">
    <h5>Ce username est déjà utilisé.</h5>
    </c:if>
    	<c:if test="${MailAlreadyExists == true}">
    <h5>Cet email est déjà utilisé.</h5>
    </c:if>
<jsp:include page="../common/footer.jsp"></jsp:include>