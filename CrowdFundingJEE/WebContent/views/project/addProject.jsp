<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../../common/header.jsp"%>

<div class="container">
    <div class="row">
        <form action="/CrowdFundingJEE/auth/addProject" method="post">
            <div class="form-group col-xs-12">
                <label for="projectTitle">Titre du projet:</label>
                <input type="text" class="form-control" id="projectTitle" name="title">
            </div>
            <div class="form-group col-xs-12 col-sm-6">
                <label for="objectiveFund">Objectif: €</label>
                <input type="number" class="form-control" id="objectiveFund" name="objective">
            </div>
            <div class="form-group col-xs-12 col-sm-6">
                <label for="resume">Resume:</label>
                <input type="text" class="form-control" id="resume"  name="resume">
            </div>
            <div class="form-group col-xs-12 col-sm-12">
                <label for="description">Description:</label>
                <textarea class="form-control" rows="10" id="description" name="description"></textarea>
            </div>
            <div class="form-group col-xs-12 col-sm-6">
                <label for="dateBegin">Date de commencement:</label>
                <input type="date" class="form-control" id="dateBegin"  name="dateBegin">
            </div>
            <div class="form-group col-xs-12 col-sm-6">
                <label for="dateEnd">Date de fin:</label>
                <input type="date" class="form-control" id="dateEnd"  name="dateEnd">
            </div>
            <div class="form-group col-xs-12 col-sm-6">
                <label for="imageUrl">URL HTTP Image:</label>
                <input type="text" class="form-control" id="imageUrl"  name="imageUrl" >
            </div>
            
            <div class="form-group col-xs-12 col-sm-6">
				<label for="prodCat">Categorie</label> 
				<select id="prodCat" class="form-control" name="category">
					<c:forEach var="item" items="${categories}">
						<option value="${item.id}">${item.name}</option>
					</c:forEach>
				</select>
			</div>

            <div class="col-xs-12">
                <button type="submit" class="btn btn-success" style="width: 100%;">Valider</button>
            </div>
        </form>
    </div>
</div>


<%@include file="../../common/footer.jsp"%>