<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="../../common/header.jsp"%>

<div class="container">
	<div class="row">

		<h1>Les projets de CrowdFunding</h1>

		<c:if test="${projects.size() > 0}">
			<c:forEach items="${projects}" var="p">
				<div class="col-xs-12 col-sm-6 col-md-4">
					<div class="project__container">
						<div class="project__title">
							<c:out value="${p.title}" />
						</div>
						<img class="project__image" src="${p.imageUrl}">
						<div class="project__description cut-words">
							<c:out value="${p.description}" />
						</div>
						<a href="/CrowdFundingJEE/showProject?id=${p.id}">Details</a>
					</div>
				</div>
			</c:forEach>
		</c:if>
		<c:if test="${projects.size() == 0}">
			<h2>Il n'y a pas encore de projets</h2>
		</c:if>

	</div>
</div>

<%@ include file="../../common/footer.jsp"%>