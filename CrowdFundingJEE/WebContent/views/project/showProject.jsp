<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ include file="../../common/header.jsp"%>

<div class="container">
	<div class="jumbotron">
		<h1><c:out value="${project.title}" /></h1>
		<h4><c:out value="${project.resume}" /></h4>
		<img src="${project.imageUrl}" alt="Image Projet" class="showProject__image">
		<p><strong>Objectifs :</strong> <c:out value="${project.currentFund}" /> / <c:out value="${project.objectiveFund}" /> &euro;</p>
		<p><strong>Date de fin :</strong><fmt:formatDate type="date" dateStyle="long" timeStyle="long" value="${project.dateEnd}" /></p>
	</div>

	<div class="row">
		<div class="showProject__description col-md-12">
			<p><c:out value="${project.description}" /></p>
		</div>
	</div>

	<c:if test="${user != null}">
		<form action="/CrowdFundingJEE/addDonation?id=${project.id}" method="post">
			<div class="form-group col-xs-12 col-sm-4">
				<label for="donation">Montant de la donation: </label>
				<input class="form-control"
					   name="donation"
					   id="donation"
					   type="number"
					   min="0"
					   step="0.5"
					   size="6"
					   required
					   onchange="(function(el){if(el.value<0){el.value=el.value/-1;}el.value=parseFloat(el.value).toFixed(2);})(this)">
			</div>
			<div class="form-group col-xs-12 col-sm-8">
				<label for="comment">Commentaire: </label>
				<textarea class="form-control"
						  name="comment"
						  id="comment"
						  rows="5"
						  required></textarea>
			</div>
			<div class="form-group col-xs-12">
				<button type="submit" class="btn" style="width: 100%">Faire une donation</button>
			</div>
		</form>

		<script>
			function setTwoNumberDecimal(event) {
				this.value = parseFloat(this.value).toFixed(2);
			}
		</script>
	</c:if>

	<div class="col-xs-12">
		<div class="panel panel-default showProject_donations">
			<div class="panel-heading">Donations</div>
			<c:if test="${donations.size() > 0}">
				<c:forEach items="${donations}" var="d">
					<ul class="list-group">
						<li class="list-group-item">
							<div class="row">
								<div class="align-left col-xs-6">
									<span class="showProject__donationValue">${d.value} €</span> par <i>${d.user.getUsername()}</i>
								</div>
								<div class="align-right col-xs-6">
									<u><fmt:formatDate type="date" dateStyle="long" timeStyle="long" value="${d.dateDonation}" /></u>
								</div>
								<div class="col-xs-12">
									<div class="showProject__donationComment cut-words">
											${d.commentary}
									</div>
								</div>
							</div>
						</li>
					</ul>
				</c:forEach>
			</c:if>
			<c:if test="${donations.size() == 0}">
				<ul class="list-group">
					<li class="list-group-item">Il n'y a encore aucune donation sur ce projet</li>
				</ul>
			</c:if>
		</div>
	</div>

<%@ include file="../../common/footer.jsp"%>
