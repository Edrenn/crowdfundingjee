<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="fr.imie.crowdfundingjee.entity.Project"
	import="fr.imie.crowdfundingjee.dao.ProjectDao"
	import="fr.imie.crowdfundingjee.entity.Category"
	import="fr.imie.crowdfundingjee.dao.CategoryDao"%>
<%@include file="../../common/header.jsp"%>

<c:set var="selected" scope="session" value="all"/>

<h1>LIST OF PROJECTS BY CATEGORIE</h1>



<select id="catSelec">
	<option value="all">All</option>
	<c:forEach items="${categories}" var="cat">
		<option value="${cat.id}">${cat.name}</option>
	</c:forEach>
</select>


<c:forEach items="${categories}" var="cat">
	<div id="${cat.id}" class="category">
		<h2 class="col-xs-12">${cat.name}</h2>
		<c:forEach items="${projects}" var="p">
			<c:if test="${p.category.name == cat.name}">	
				<div class="col-xs-12 col-sm-6 col-md-4">
					<div class="project__container">
						<div class="project__title">
							<c:out value="${p.title}" />
						</div>
						<img class="project__image" src="${p.imageUrl}">
						<div class="project__description">
							<c:out value="${p.description}" />
						</div>
						<a href="/CrowdFundingJEE/showProject?id=${p.id}">Details</a>
					</div>
				</div>
			</c:if>
		</c:forEach>
	</div>
	
</c:forEach>


<script>

	$(document).ready(function()
	{
		$("#catSelec").change(function()
		{
			
			var selected = $(this).find("option:selected").val();
			console.log(selected);
			
			if(selected == 'all')
			{
				$(".category").fadeIn(500);
			} else
			{
				$(".category:not(#"+selected+")").fadeOut(500, function()
				{
					$('#'+selected).fadeIn(500);		
				});	
			}
			
		});
	});

</script>


<%@include file="../../common/footer.jsp"%>