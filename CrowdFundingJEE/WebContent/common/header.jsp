<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>${title}</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet"
          href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
          crossorigin="anonymous">
    <c:url var="styleSheetUrl" value="/style.css" />
    <link rel="stylesheet" href="${styleSheetUrl}">
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
</head>
<body>
    <nav class="navbar navbar-default">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/CrowdFundingJEE/">CrowdFunding</a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                    <li><a href="/CrowdFundingJEE/"><span class="glyphicon glyphicon-home" aria-hidden="true"></span></a></li>
                    <li><a href="/CrowdFundingJEE/listProjectByCategory">Catégories</a></li>
                    <li><a href="/CrowdFundingJEE/auth/addProject">Créer un projet</a></li>

                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <c:if test="${user != null}">
                        <li><a href="/CrowdFundingJEE/auth/profile"><span class="glyphicon glyphicon-user" aria-hidden="true"></span></a></li>
                        <li><a href="/CrowdFundingJEE/logout"><span class="glyphicon glyphicon-off" aria-hidden="true"></span></a></li>
                    </c:if>
                    <c:if test="${user == null}">
                        <li><a href="/CrowdFundingJEE/login">Connexion</a></li>
                        <li><a href="/CrowdFundingJEE/subscribe">Inscription</a></li>
                    </c:if>
                </ul>
            </div><!--/.nav-collapse -->
        </div>
    </nav>