package fr.imie.crowdfundingjee.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.imie.crowdfundingjee.dao.DaoFactory;
import fr.imie.crowdfundingjee.entity.Project;

@SuppressWarnings("serial")
@WebServlet(urlPatterns = "/listProject")
public class ListProjectServlet extends HttpServlet{

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// get all products from database
		List<Project> projects = DaoFactory.getProjectDao().findAll();
		req.setAttribute("projects", projects);

		req.getRequestDispatcher("/views/project/listProject.jsp").forward(req, resp);
	}
}
