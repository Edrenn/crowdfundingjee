package fr.imie.crowdfundingjee.servlet;

import fr.imie.crowdfundingjee.dao.DaoFactory;
import fr.imie.crowdfundingjee.dao.jpa.JpaProjectDao;
import fr.imie.crowdfundingjee.entity.Donation;
import fr.imie.crowdfundingjee.entity.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Date;

@WebServlet(urlPatterns = "/addDonation")
public class AddDonationServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Long id = Long.parseLong(req.getParameter("id"));
        Float newFund = Float.parseFloat(req.getParameter("donation"));
        String comment = req.getParameter("comment");

        if (newFund > 0) {
            boolean result = DaoFactory.getProjectDao().updateCurrentFund(id, newFund);
            if (result) {
                Donation donation = new Donation();
                donation.setProject(DaoFactory.getProjectDao().findById(id));
                donation.setCommentary(comment);
                donation.setDateDonation(new Date());
                donation.setValue(newFund);
                HttpSession session = req.getSession();
                User user = (User) session.getAttribute("user");
                donation.setUser(user);

                DaoFactory.getDonationDao().insertDonation(donation);
            }
        }

        resp.sendRedirect("/CrowdFundingJEE/showProject?id="+id);
    }

}
