package fr.imie.crowdfundingjee.servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.imie.crowdfundingjee.dao.DaoFactory;
import fr.imie.crowdfundingjee.entity.Project;
import fr.imie.crowdfundingjee.entity.User;

/**
 * Servlet implementation class UserServlet
 */
@WebServlet("/auth/profile")
public class UserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	HttpSession session;
	EntityManagerFactory emf = null;
	
	@Override
	public void init(ServletConfig config) throws ServletException {
		emf = Persistence.createEntityManagerFactory("My-PU");
	}
	
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.session = request.getSession();
		this.session.setAttribute("MDPFailed", false);
		this.session.setAttribute("MailAlreadyExists", false);
		RequestDispatcher rd = 
				request.getRequestDispatcher("/views/UserPage.jsp");
		rd.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest req , HttpServletResponse resp ) throws ServletException, IOException {
		ArrayList<String> values = new ArrayList<String>();
		String[] names = {"password", "verifPassword", "email", "firstname", "lastname", "imageUrl"};


		if (req.getParameter("password").equals(req.getParameter("verifPassword"))) {
			this.session.setAttribute("MDPFailed", false);
			for(int i = 0; i < names.length; i++)
			{
	
					values.add(req.getParameter(names[i]));
			}
			
			if(values.size() == names.length)
			{
				
				User author = (User)session.getAttribute("user");
		        User userTest = DaoFactory.getUserDao().findByMail(values.get(2));
	        	if (userTest == null || userTest.getEmail().equals(author.getEmail())) {
					User newUser = new User(values);
					if (newUser.getPassword().equals("") || newUser.getPassword().equals(null)) {
						newUser.setPassword(author.getPassword());
					}
					newUser.setId(author.getId());
					newUser.setUsername(author.getUsername());
			        DaoFactory.getUserDao().update(newUser);
			        this.session.setAttribute("user", newUser);
		            resp.sendRedirect("/CrowdFundingJEE/listProject");
	        	}
	        	else{

		    		session.setAttribute("MailAlreadyExists", true);
					RequestDispatcher rd = 
							req.getRequestDispatcher("/views/UserPage.jsp");
					rd.forward(req, resp);
	        	}
			}
		}
		else{
			session.setAttribute("MDPFailed", true);
			RequestDispatcher rd = 
					req.getRequestDispatcher("/views/UserPage.jsp");
			rd.forward(req, resp);
		}

		
		
	}

}
