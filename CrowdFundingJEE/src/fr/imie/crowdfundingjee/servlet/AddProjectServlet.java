package fr.imie.crowdfundingjee.servlet;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.imie.crowdfundingjee.entity.Category;
import fr.imie.crowdfundingjee.entity.Project;
import fr.imie.crowdfundingjee.entity.User;
import fr.imie.crowdfundingjee.dao.DaoFactory;


import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@WebServlet(urlPatterns = "/auth/addProject")
public class AddProjectServlet extends HttpServlet {
    
	
	@Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    	List<Category> categories = DaoFactory.getCategoryDao().findAll();
		
		req.getSession().setAttribute("categories", categories);
		
		RequestDispatcher rd = 
				req.getRequestDispatcher("/views/project/addProject.jsp");
		rd.forward(req, resp);
    }

    
    @Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    	
    	HttpSession session = req.getSession();
    	
		Project project = new Project();
		
		User author = (User)session.getAttribute("user");
		
		project.setAuthor(author);
		project.setTitle(req.getParameter("title"));
		project.setDescription(req.getParameter("description")); 
		project.setObjectiveFund(Float.parseFloat(req.getParameter("objective")));
		project.setResume(req.getParameter("resume"));
		String dateBegin = req.getParameter("dateBegin");
		
		System.out.println(dateBegin);
		
		//Date format
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Date date1 = new Date();
		try {
			date1 = sdf.parse(dateBegin);
		} catch (ParseException e1) {
			System.out.println(date1);
			date1 = null;
		}
		project.setDateBegin(date1);
		String dateEnd = req.getParameter("dateEnd");
		
		Date date2;
		try {
			date2 = sdf.parse(dateEnd);
		} catch (ParseException e) {
			date2 = null;
		}
		project.setDateEnd(date2);
		
		project.setImageUrl(req.getParameter("imageUrl"));
		//Creer en base de donn�es une categorie avec l'id 1 pour le moment
		Category category = DaoFactory.getCategoryDao().findById(Long.parseLong(req.getParameter("category")));
		project.setCategory(category);
		
		Long id = DaoFactory.getProjectDao().insertProject(project);
		
		resp.sendRedirect("/CrowdFundingJEE/listProject");
		//resp.sendRedirect("/CrowdFundingJEE/showProject?id=" + id.toString());
	}
}
