package fr.imie.crowdfundingjee.servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.imie.crowdfundingjee.dao.DaoFactory;
import fr.imie.crowdfundingjee.entity.User;

/**
 * Created by Kiki on 29/06/2017.
 */
@SuppressWarnings("serial")
@WebServlet(urlPatterns = "/subscribe")
public class SubscriptionServlet extends HttpServlet {
	
	EntityManagerFactory emf = null;
	HttpSession session;
	
	@Override
	public void init(ServletConfig config) throws ServletException {
		emf = Persistence.createEntityManagerFactory("My-PU");
	}
	
	@Override
	public void destroy() {
		emf.close();
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		session = req.getSession();
		session.setAttribute("MailAlreadyExists", false);
		session.setAttribute("LoginAlreadyExists", false);
		req.getRequestDispatcher("/views/subscribe.jsp").forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		ArrayList<String> values = new ArrayList<String>();
		String[] names = {"login", "password", "email", "firstname", "lastname", "imageUrl"};
		
		for(int i = 0; i < names.length; i++)
		{
			if(req.getParameter(names[i]).equals(""))
			{
				resp.getWriter().print("The field ");
                for (int j = i; j < names.length; j++) 
                {
                    resp.getWriter().print(names[j]);
                    if (j < (names.length - 1)) 
                    {
                        resp.getWriter().print(", ");
                    }
                }
                resp.getWriter().println(" is empty.");
                break;
			}else
			{
				values.add(req.getParameter(names[i]));
			}
		}
		
		if(values.size() == names.length)
		{

	        User userTest = DaoFactory.getUserDao().findByLogin(values.get(0));
	        if (userTest == null) {
	    		session.setAttribute("LoginAlreadyExists", false);
	        	

		        userTest = DaoFactory.getUserDao().findByMail(values.get(2));
	        	if (userTest == null) {
		    		session.setAttribute("MailAlreadyExists", false);
					User newUser = new User(values);
					
					EntityManager em = emf.createEntityManager();
		    		EntityTransaction t = em.getTransaction();
		    		try {
			    		t.begin();
			    		em.persist(newUser);
			    		t.commit();
		    		} finally {
		    		if (t.isActive()) t.rollback();
		    			em.close();
		    		}
		    		session.setAttribute("user", newUser);
		            resp.getWriter().println("Inscription réussit !");
					resp.sendRedirect(req.getContextPath()+"/home");
				}
		        else{
		    		session.setAttribute("MailAlreadyExists", true);
		    		req.getRequestDispatcher("/views/subscribe.jsp").forward(req, resp);
				
				
			}
	        }
	        else{
	    		session.setAttribute("LoginAlreadyExists", true);
	    		req.getRequestDispatcher("/views/subscribe.jsp").forward(req, resp);
	        }
		}
	}
}
