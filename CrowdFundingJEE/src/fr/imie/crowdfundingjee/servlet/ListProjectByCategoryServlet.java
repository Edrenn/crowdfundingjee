package fr.imie.crowdfundingjee.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.imie.crowdfundingjee.dao.DaoFactory;
import fr.imie.crowdfundingjee.entity.Category;
import fr.imie.crowdfundingjee.entity.Project;

@SuppressWarnings("serial")
@WebServlet(urlPatterns = "/listProjectByCategory")
public class ListProjectByCategoryServlet extends HttpServlet{

	@Override
	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// get all products from database
				List<Project> projects = DaoFactory.getProjectDao().findAll();
				List<Category> categories = DaoFactory.getCategoryDao().findAll();
				
				req.setAttribute("projects", projects);
				req.setAttribute("categories", categories);
				
				RequestDispatcher rd = 
						req.getRequestDispatcher("/views/project/listProjectByCategory.jsp");
				rd.forward(req, resp);
	}
}
