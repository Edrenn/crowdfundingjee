package fr.imie.crowdfundingjee.servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

import fr.imie.crowdfundingjee.dao.DaoFactory;
import fr.imie.crowdfundingjee.entity.User;

@WebServlet(urlPatterns = "/login")
public class LoginServlet extends HttpServlet {
	
	HttpSession session;
	
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.session = req.getSession();
        this.session.setAttribute("failed", false);
        if (session.getAttribute("login") != null) {
            resp.sendRedirect("/CrowdFundingJEE/listProject");
        } else {
            req.getRequestDispatcher("/views/LoginPage.jsp").forward(req, resp);
        }

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String login = req.getParameter("login");
        String password = req.getParameter("password");
        PrintWriter writer = resp.getWriter();

        User userTest = DaoFactory.getUserDao().find(login,password);
        
        if (userTest != null) {
            session.setAttribute("user", userTest);
            resp.sendRedirect("/CrowdFundingJEE/listProject");
        } else {
            HttpSession session = req.getSession();
            //resp.sendRedirect("/CrowdFundingJEE/login");
        	session.setAttribute("failed", true);
            req.getRequestDispatcher("/views/LoginPage.jsp").forward(req, resp);
        }
    }
}
