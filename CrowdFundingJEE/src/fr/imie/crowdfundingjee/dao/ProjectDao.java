package fr.imie.crowdfundingjee.dao;

import java.util.List;

import fr.imie.crowdfundingjee.entity.Project;

public interface ProjectDao {
	List<Project> findAll();
	Long insertProject(Project project);
	Project findById(Long id);
	boolean updateCurrentFund(Long id, Float donation);
}
