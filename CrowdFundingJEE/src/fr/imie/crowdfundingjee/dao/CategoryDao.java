package fr.imie.crowdfundingjee.dao;

import java.util.List;

import fr.imie.crowdfundingjee.entity.Category;

public interface CategoryDao {
	List<Category> findAll();
	Category findById(long i);
}
