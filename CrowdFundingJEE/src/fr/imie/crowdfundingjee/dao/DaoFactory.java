package fr.imie.crowdfundingjee.dao;

import fr.imie.crowdfundingjee.dao.jpa.JpaDonationDao;
import fr.imie.crowdfundingjee.dao.jpa.JpaProjectDao;
import fr.imie.crowdfundingjee.dao.jpa.JpaCategoryDao;
import fr.imie.crowdfundingjee.dao.jpa.JpaUserDao;
import fr.imie.crowdfundingjee.util.PersistenceManager;

public class DaoFactory {

	// Private constructor prevent instantiation
	private DaoFactory() {
	}

	public static ProjectDao getProjectDao() {
		return new JpaProjectDao(PersistenceManager.getEntityManagerFactory());
	}
	
	public static UserDao getUserDao() {
		return new JpaUserDao(PersistenceManager.getEntityManagerFactory());
	}

	public static CategoryDao getCategoryDao() {
		return new JpaCategoryDao(PersistenceManager.getEntityManagerFactory());
	}

	public static DonationDao getDonationDao() { return new JpaDonationDao(PersistenceManager.getEntityManagerFactory()); }
}
