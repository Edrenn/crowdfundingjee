package fr.imie.crowdfundingjee.dao;


import fr.imie.crowdfundingjee.entity.User;



public interface UserDao {
	User find(String userUsername, String userPwd);
	User findByLogin(String userUsername);
	User findByMail(String userMail);
	void update(User user);
}
