package fr.imie.crowdfundingjee.dao.jpa;

import javax.persistence.EntityManagerFactory;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;

import fr.imie.crowdfundingjee.dao.ProjectDao;
import fr.imie.crowdfundingjee.entity.Project;

public class JpaProjectDao extends JpaDao implements ProjectDao {
	
	public JpaProjectDao(EntityManagerFactory emf) {
		super(emf);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Project> findAll() {
		List<Project> projects;
		EntityManager em = getEntityManagerFactory().createEntityManager();

		Query query = em.createNamedQuery("project");

		projects = (List<Project>) query.getResultList();

		em.close();

		return projects;
	}

	@Override
	public Long insertProject(Project project) {
		EntityManager em = getEntityManagerFactory().createEntityManager();
		EntityTransaction t = em.getTransaction();

		// Use an EntityManager to persist the object
		try {

			t.begin();

			em.persist(project);
			em.flush();
			
			t.commit();

		} finally {
			if (t.isActive())
				t.rollback();
			em.close();

		}

		return project.getId();
	}

	@Override
	public Project findById(Long id) {
		Project project;
		EntityManager em = getEntityManagerFactory().createEntityManager();

		project = em.find(Project.class, id);

		em.close();

		return project;
	}

	public boolean updateCurrentFund(Long id, Float donation) {
		EntityManager em = getEntityManagerFactory().createEntityManager();
		EntityTransaction t = em.getTransaction();

		try {
			t.begin();
			Query query = em.createQuery("UPDATE Project p SET p.currentFund = p.currentFund + :donation WHERE p.id = :id");
			query.setParameter("donation", donation);
			query.setParameter("id", id);
			query.executeUpdate();
			t.commit();
			em.close();
			return true;
		} catch (Exception e) {
			System.out.println(e);
			em.close();
			return false;
		}
	}
}
