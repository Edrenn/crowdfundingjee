package fr.imie.crowdfundingjee.dao.jpa;

import javax.persistence.EntityManagerFactory;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;

import fr.imie.crowdfundingjee.dao.UserDao;
import fr.imie.crowdfundingjee.entity.User;



public class JpaUserDao  extends JpaDao implements UserDao {
	EntityManagerFactory emf;
	public JpaUserDao(EntityManagerFactory emf) {
		super(emf);
		this.emf = emf;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public User find(String userUsername, String userPwd) {
		EntityManager em = getEntityManagerFactory().createEntityManager();
		User user;
		Query query = em.createQuery("SELECT u FROM User AS u WHERE u.username = :userUsername AND u.password = :userPwd");
		query.setParameter("userUsername",userUsername);
		query.setParameter("userPwd",userPwd);
		
		try {
			user = (User) query.getSingleResult();
			
		} catch (Exception e) {
			user = null;
		}
			em.close();

			return user;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public User findByLogin(String userUsername) {
		EntityManager em = getEntityManagerFactory().createEntityManager();
		User user;
		Query query = em.createQuery("SELECT u FROM User AS u WHERE u.username = :userUsername");
		query.setParameter("userUsername",userUsername);
		
		try {
			user = (User) query.getSingleResult();
			
		} catch (Exception e) {
			user = null;
		}
			em.close();

			return user;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public User findByMail(String userMail) {
		EntityManager em = getEntityManagerFactory().createEntityManager();
		User user;
		Query query = em.createQuery("SELECT u FROM User AS u WHERE u.email = :email");
		query.setParameter("email",userMail);
		
		try {
			user = (User) query.getSingleResult();
			
		} catch (Exception e) {
			user = null;
		}
			em.close();

			return user;
	}
	
	public void update(User user){
		
		EntityManager em = emf.createEntityManager();
		EntityTransaction t = em.getTransaction();
		try {
    		t.begin();
    		em.merge(user);
    		t.commit();
		} finally {
		if (t.isActive()) t.rollback();
			em.close();
		}

	}

}
