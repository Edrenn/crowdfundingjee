package fr.imie.crowdfundingjee.dao.jpa;

import fr.imie.crowdfundingjee.dao.DonationDao;
import fr.imie.crowdfundingjee.entity.Donation;
import fr.imie.crowdfundingjee.entity.Project;
import fr.imie.crowdfundingjee.entity.User;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import java.util.List;

public class JpaDonationDao extends JpaDao implements DonationDao {

	public JpaDonationDao(EntityManagerFactory emf) {
		super(emf);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Donation> findByProject(Project project) {
		List<Donation> donations;
		EntityManager em = getEntityManagerFactory().createEntityManager();

		Query query = em.createQuery("SELECT d FROM Donation d WHERE d.project = :project");
		query.setParameter("project", project);

		donations = (List<Donation>) query.getResultList();

		em.close();

		return donations;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Donation> findByUser(User user) {
		List<Donation> donations;
		EntityManager em = getEntityManagerFactory().createEntityManager();

		Query query = em.createQuery("SELECT d FROM Donation d WHERE d.user = :user");
		query.setParameter("user", user);

		donations = (List<Donation>) query.getResultList();

		em.close();

		return donations;
	}

	@Override
	public Long insertDonation(Donation donation) {
		EntityManager em = getEntityManagerFactory().createEntityManager();
		EntityTransaction t = em.getTransaction();

		// Use an EntityManager to persist the object
		try {

			t.begin();

			em.persist(donation);
			em.flush();
			
			t.commit();

		} finally {
			if (t.isActive())
				t.rollback();
			em.close();

		}

		return donation.getId();
	}

	@Override
	public Donation findById(Long id) {
		Donation donation;
		EntityManager em = getEntityManagerFactory().createEntityManager();

		donation = em.find(Donation.class, id);

		em.close();

		return donation;
	}
}
