package fr.imie.crowdfundingjee.dao.jpa;

import javax.persistence.EntityManagerFactory;

public class JpaDao {
	private EntityManagerFactory emf;

    protected JpaDao(EntityManagerFactory emf) {
        this.emf = emf;
    }

	public EntityManagerFactory getEntityManagerFactory() {
        return emf;
    }

    public void close() {
        emf.close();
    }
}
