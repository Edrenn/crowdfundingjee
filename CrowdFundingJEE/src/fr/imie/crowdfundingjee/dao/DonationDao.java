package fr.imie.crowdfundingjee.dao;

import java.util.List;

import fr.imie.crowdfundingjee.entity.Donation;
import fr.imie.crowdfundingjee.entity.Project;
import fr.imie.crowdfundingjee.entity.User;

public interface DonationDao {
    List<Donation> findByProject(Project project);
    List<Donation> findByUser(User user);
    Long insertDonation(Donation donation);
    Donation findById(Long id);
}
